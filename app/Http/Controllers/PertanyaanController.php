<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use App\Profil;
Use Auth;
use App\Tag;
use App\Exports\PertanyaanExport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;

class PertanyaanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth') -> except(['index','export']) ;
    }

    public function index(){
        // $data_pertanyaan = DB::table('pertanyaan')
        //                     ->join('profil','pertanyaan.profil_id','=','profil.id')
        //                     ->select('pertanyaan.*','profil.nama_lengkap')
        //                     ->orderBy('pertanyaan.id', 'asc')
        //                     ->get();

        $user = Auth::user();

        $data_pertanyaan = $user -> pertanyaan;
        // dd($data_pertanyaan[0]->user->name);
        return view('pertanyaan.index', compact('data_pertanyaan'));
    }

    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all());
        date_default_timezone_set('Asia/Jakarta');
        $request->validate([
            'judul_pertanyaan' => 'required|max:255',
            'isi_pertanyaan' => 'required'
        ]);
        // $query = DB::table('pertanyaan')->insertGetId(
        //     ['profil_id' => $request['profil_id'],
        //     'judul' => $request['judul_pertanyaan'],
        //     'isi'=> $request['isi_pertanyaan'],
        //     'tanggal_dibuat' => date('y/m/d')
        //     // 'profil_id' => $request['inputProfilId']
        // ]);
        // return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!')  ;

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan -> profil_id = $request['profil_id'];
        // $pertanyaan -> judul = $request['judul_pertanyaan'];
        // $pertanyaan -> isi = $request['isi_pertanyaan'];
        // $pertanyaan -> tanggal_dibuat = date('y/m/d');
        // $pertanyaan -> save();

        $tags_arr = explode(',',$request['tag']);

        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::firstOrCreate(['tag' => $tag_name]);
            $tag_ids[] = $tag->id;
            // $tag = Tag::where('tag',$tag_name)->first();
            // if($tag){
            //     $tag_ids[] = $tag->id;
            // } else {
            //     $new_tag = Tag::create(["tag"=>$tag_name]);
            //     $tag_ids[] = $new_tag->id;
            // }
        }
        // dd($tag_ids);

        $user = Auth::user();

        $pertanyaan = $user->pertanyaan() -> create([
            'judul' => $request['judul_pertanyaan'],
            'isi' => $request['isi_pertanyaan'],
            'tanggal_dibuat' => date('y/m/d')

        ]);

        $pertanyaan->tag()->sync($tag_ids);


        Alert::success('Berhasil', 'Pertanyaan berhasil disimpan!');

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function update($id, Request $request){
        // dd($request->all());
        $request->validate([
            'judul_pertanyaan' => 'required|max:255',
            'isi_pertanyaan' => 'required'
        ]);
        // $query = DB::table('pertanyaan')
        //     -> where('id',$id)
        //     -> update([
        //         'judul' => $request['judul_pertanyaan'],
        //         'isi'=> $request['isi_pertanyaan'],
        //         'tanggal_diperbarui' => date('y/m/d')
        //     ]);
        $pertanyaan = Pertanyaan::find($id) -> update ([
            'judul' => $request['judul_pertanyaan'],
            'isi' => $request['isi_pertanyaan'],
            'tanggal_diperbarui' => date('y/m/d'),
        ]);
        return redirect('/pertanyaan')->with('success', 'Data pertanyaan berhasil diubah!')  ;
    }

    public function show($id){
        // $pertanyaan = DB::table('pertanyaan')
        //             -> join('profil','pertanyaan.profil_id','=','profil.id')
        //             -> select('pertanyaan.*','profil.nama_lengkap','profil.foto')
        //             -> where('pertanyaan.id',$id)
        //             -> first();

        $pertanyaan = Pertanyaan::find($id);
        // dd($pertanyaan->tag()->select('tag')->get());
        return view('pertanyaan.show',compact('pertanyaan'));
    }

    public function edit($id){
        // $pertanyaan = DB::table('pertanyaan')
        //             -> join('profil','pertanyaan.profil_id','=','profil.id')
        //             -> select('pertanyaan.*','profil.nama_lengkap','profil.foto')
        //             -> where('pertanyaan.id',$id)
        //             -> first();

        $pertanyaan = Pertanyaan::with('user')->find($id);
        $tags_arr = [];
        foreach($pertanyaan -> tag as $tag){
            $tags_arr[] = $tag->tag;
        }
        return view('pertanyaan.edit',compact('pertanyaan','tags_arr'));
    }

    public function destroy($id){
        // $pertanyaan = DB::table('pertanyaan')
        //             -> where('id',$id)
        //             -> delete();

        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Data berhasil dihapus!')  ;
    }

    public function export()
    {
        return Excel::download(new PertanyaanExport, 'pertanyaan.xlsx');
    }

}
