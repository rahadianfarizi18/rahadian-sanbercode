<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function pertanyaan(){
        return $this->hasOne('App\Pertanyaan','jawaban_tepat_id');
    }

}
