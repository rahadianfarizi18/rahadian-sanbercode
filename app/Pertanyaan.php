<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo('App\User','profil_id');
    }

    public function jawabanTepat(){
        return $this->hasOne('App\Jawaban','jawaban_tepat_id');
    }

    public function tag(){
        return $this->belongsToMany('App\Tag');
    }
}
