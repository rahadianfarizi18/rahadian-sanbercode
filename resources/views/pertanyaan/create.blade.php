@extends('adminlte.master')

@push('script-head')

@endpush
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Buat Pertanyaan Baru</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">General Form</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<form role="form" method="POST" action="/pertanyaan">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="inputProfilId">Profil ID</label>
            <input type="text" class="form-control" name= "profil_id" id="inputProfilId" placeholder="Profil ID" value="{{ Auth::id() }}" disabled>
        </div>
        <div class="form-group">
            <label for="userName">Nama Lengkap</label>
            <input type="text" class="form-control" name= "userName" id="userName" placeholder="Nama Lengkap" value="{{ Auth::user() -> name }}" disabled>
        </div>
        <div class="form-group">
            <label for="judulPertanyaan">Judul</label>
            <input type="text" class="form-control" name="judul_pertanyaan" id="judulPertanyaan" placeholder="Judul pertanyaan" value="{{old('judul_pertanyaan','')}}">
            @error('judul_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tag">Tags</label>
            <input data-role = "tagsinput" type='text' class="form-control" name="tag" id="tag" placeholder="Tags..." value="{{old('tag','')}}">
            @error('tag')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi_pertanyaan">Pertanyaan</label>
            <textarea id="isi_pertanyaan" name="isi_pertanyaan" class="my-editor form-control" rows="15">{!! old('isi_pertanyaan', '') !!}</textarea>
            @error('isi_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

@endsection

@push('script')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>

@endpush
