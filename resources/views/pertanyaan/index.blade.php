@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="card-title">Data Pertanyaan</h1>
            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
            @if(session('success'))
                <div class="alert alert-success ml-2" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-2 ml-2 mt-2" href=" {{route('pertanyaan.create')}} ">Buat pertanyaan baru</a>
            <table class="table table-head-fixed">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Profil ID</th>
                        <th>Nama</th>
                        <th>Judul</th>
                        <th>Pertanyaan</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diperbarui</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data_pertanyaan as $key => $value)
                    <tr>
                        <td> </td>
                        <td> {{ $value -> profil_id }} </td>
                        <td> {{ $value -> user['name'] }} </td>
                        <td> {{ $value -> judul }} </td>
                        <td> {!! $value -> isi !!} </td>
                        <td> {{ $value -> tanggal_dibuat }} </td>
                        <td> {{ $value -> tanggal_diperbarui }} </td>
                        <td style="display:flex">
                            <a href=" {{route('pertanyaan.show', ['pertanyaan' => $value->id] )}} "  type="button" class="btn btn-primary btn-sm">show</a>
                            <a href=" {{route('pertanyaan.edit', ['pertanyaan' => $value->id] )}} "  type="button" class="btn btn-dark btn-sm ml-1">edit</a>
                            <form action=" {{route('pertanyaan.destroy', ['pertanyaan' => $value->id] )}} " method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm ml-1">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>


@endsection
