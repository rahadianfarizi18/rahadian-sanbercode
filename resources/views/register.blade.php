<!DOCTYPE html> 
<html>
    <head>
        <h1>Buat Account Baru!</h1>
    </head>
    <body>
        <h2>Sign Up Form</h2>
        <form action="/form_post" method="POST">
            @csrf
            <label for="first_name">First Name:</label>
            <br><br>
            <input type="text" name="first_name">
            <br><br>
            <label for="last_name">Last Name:</label>
            <br><br>
            <input type="text" name="last_name">
            <br><br>
            <label for="gender">Gender:</label>
            <br><br>
            <input type="radio" name="gender" id="gender">Male<br>
            <input type="radio" name="gender" id="gender">Female<br>
            <input type="radio" name="gender" id="gender">Other<br>
            <br><br>
            <label for="nationality">Nationality:</label>
            <br><br>
            <select name="nationality" id="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="wna">WNA</option>
            </select>
            <br><br>
            <label for="bio">Bio:</label><br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br><br>
            <input type="submit" value="Sign up">
        </form>   
    </body>    
</html>